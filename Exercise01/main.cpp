#include <iostream>
#include <Windows.h>

using namespace std;

int main() {

	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	
	const int lim = 500;
	int amount;
	while ((cout << "������� ����� ����� �� 500: ") && !(cin >> amount) || (amount < 0 || amount > lim))
	{
		cout << "������� ���������� �����: " << endl;
		cin.clear();
		cin.ignore();
	}

	system("cls");
	
	for (size_t i = amount; i < lim; i++)
		amount += (i + 1);
	

	cout << "����� ����� = " << amount;

	return 0;
}