#include <iostream>

using namespace std;

int main() {

	const int lim = 1000;
	int avg = 0;

	for (size_t i = 0; i < lim; avg += (i++ + 1));

	cout << "avg = " << (double)avg / lim;

	return 0;
}