#include <iostream>
#include <Windows.h>
#include <iomanip>

using namespace std;

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	int number;
	while ((cout << "������� ����� �������� �� 1 �� 9: ") && !(cin >> number) | (number < 1) || (number > 9))
	{
		cout << "������� ���������� ����� ��������." << endl;
		cin.clear();
		cin.ignore();
	}
	cout << endl;

	for (size_t i = 0; i < 10; i++)
		cout << number << " x " << setw(2) << i + 1 << " = " << number * (i + 1) << endl;

	return 0;
}