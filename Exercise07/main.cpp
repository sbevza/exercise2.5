#include <iostream>
#include <Windows.h>

using namespace std;

int main() {

	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	int number1, number2, buffer;

	while ((cout << "������� ������������ �������� ����� �����: ") && !(cin >> number1 >> number2))
	{
		cout << "������� ���������� ������ �����!" << endl;
		cin.clear();
		cin.ignore();
	}

	if (number1 > number2) {
		buffer = number1;
		number1 = number2;
		number2 = buffer;
	}

	bool exit = false;
	while (!exit)
	{
		cout << "�������� ��������:" << endl;
		cout << "1. ������� ��� ������ ����� �� ���������;" << endl;
		cout << "2. ������� ��� �������� ����� �� ���������;" << endl;
		cout << "3. ������� ��� �����, ������� ����." << endl;
		cout << "������� 0 ��� ������" << endl;

		int choice;
		while ((cout << "�����: ") && !(cin >> choice) || (choice < 0) || (choice > 3))
		{
			cout << "�������� ���������� �����..." << endl;
			cin.clear();
			cin.ignore();
		}
		
		system("cls");
		if (choice == 0)
		{
			exit = true;
			break;
		}

		for (int i = number1; i <= number2; i++)
		{
			switch (choice) {
			case 1:
				if (i % 2 == 0)
					cout << i << " ";
				break;
			case 2:
				if (i % 2 == 1)
					cout << i << "  ";
				break;
			case 3:
				if (i % 7 == 0)
					cout << i << "  ";
				break;
			}
		}

		cout << endl;
	}
	return 0;
}