#include <iostream>
#include <Windows.h>

using namespace std;

int main() {

	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	const int lim = 20;
	int number;

	while ((cout << "������� ����� �� 1 �� " << lim << ": ") && !(cin >> number) || (number < 1) || (number > lim))
	{
		cout << "������� ���������� �����!" << endl;
		cin.clear();
		cin.ignore();
	}

	int mpc = 1;
	for (size_t i = number; i < lim + 1; mpc *= i++);
	
	cout << "������������ = " << mpc;

	return 0;
}